#include "InputHandler.h"

#include <iostream>

#include "LeftCommand.h"
#include "Hero.h"

InputHandler::InputHandler(std::unique_ptr <Hero>  heroSS)
{
    this->hero = std::move( heroSS );

    // TAKA OMPLEMENTACJA KONCZY SIE po jednokrotnym u�yciu :) 
    //this->left = std::unique_ptr <Command>( new LeftCommand( hero ) );
    //this->back = std::unique_ptr <Command>( new BackCommand( hero ) );
    //this->right = std::unique_ptr <Command>( new RightCommand(  hero  ) );
    //this->forward = std::unique_ptr <Command>( new ForwardCommand(  hero  ) );
    //this->notMoved = std::unique_ptr <Command>( new NotMovedCommand( hero  ) );

    //  TAKA KONCZY SIE IMPLEMENTACJA je�li hero by�by unique_ptr
    //    Hero constr 02392DF8
    //    Hero action left 02392DF8
    //    Hero action back 00000000
    //    Hero action right 00000000
    //    Hero action forward 00000000
    //    Hero action forward 00000000
    //    destruktor inputHandler
    //    Hero destr 02392DF8
    //    Aby kontynuowa�, naci�nij dowolny klawisz . . .


    //std::make_unique
}

InputHandler::~InputHandler()
{
    std::cout << "destruktor inputHandler " << std::endl;
}

std::unique_ptr <Command> InputHandler::inputHandler(char key)
{
    switch ( key )
    {
    case 'a':
    {
     //   this->left = ;
        return std::move(std::unique_ptr <Command>( new LeftCommand( hero ) ));
    }
    case 's':
    {
    //    this->back = std::unique_ptr <Command>( new BackCommand( hero ) );
     //   return std::move( new BackCommand (hero) ); // nie zadzia�a bo new BackCommand nie jest typem kt�ry zwracamy 
        return std::move( std::unique_ptr <Command>( new BackCommand( hero ) ));
    }
    case 'd':
    {
        auto right = std::unique_ptr <Command>( new RightCommand( hero ) );
        return std::move( right );
    }
    case 'w':
    {
        auto forward = std::unique_ptr <Command>( new ForwardCommand( hero ) );
        return std::move( forward );
    }
    default:
        auto notMoved = std::unique_ptr <Command>( new NotMovedCommand( hero ) );
        return std::move( notMoved );
    }

}
