#pragma once

#include <memory>

class Command;
class Hero; 

class InputHandler
{
    std::shared_ptr <Hero> hero;

    //std::unique_ptr <Command>  left;
    //std::unique_ptr <Command>  right;
    //std::unique_ptr <Command>  back;
    //std::unique_ptr <Command>  forward;
    //std::unique_ptr <Command>  notMoved;

public:


    InputHandler(std::unique_ptr< Hero > hero );

    virtual ~InputHandler();

    std::unique_ptr <Command> inputHandler( char key );
};

