#include "LeftCommand.h"

#include <iostream>


LeftCommand::LeftCommand( std::shared_ptr<Hero> her ):hero(std::move(her))
{
    std::cout << " LeftCommand  Constr " << std::endl;
}



LeftCommand::~LeftCommand()
{
    std::cout << " ~LeftCommand " << std::endl;
}

void LeftCommand::execute()
{
    hero->left();
}

BackCommand::BackCommand( std::shared_ptr<Hero> her ) :hero( std::move( her ))
{
}

BackCommand::BackCommand( BackCommand && her ) : hero( std::move( her.hero ) )
{
}

BackCommand::~BackCommand()
{
    std::cout << " ~BackCommand " << std::endl;

}

void BackCommand::execute()
{
    hero->back();
}

ForwardCommand::ForwardCommand( std::shared_ptr<Hero> her ) :hero( std::move( her ))
{
}

ForwardCommand::ForwardCommand( ForwardCommand && her ) : hero( std::move( her.hero ) )
{
}

ForwardCommand::~ForwardCommand()
{
    std::cout << " ~ForwardCommand " << std::endl;

}

void ForwardCommand::execute()
{
    hero->forward();

}

RightCommand::RightCommand( std::shared_ptr<Hero> her ) :hero( std::move( her ))
{
}

RightCommand::RightCommand( RightCommand && her ): hero (std::move(her.hero))
{
}

RightCommand::~RightCommand()
{
    std::cout << " ~RightCommand " << std::endl;

}

void RightCommand::execute()
{
    hero->right();

}

NotMovedCommand::NotMovedCommand( std::shared_ptr<Hero> her ) :hero( std::move( her ) )
{
}

NotMovedCommand::NotMovedCommand( NotMovedCommand && her ) : hero( std::move( her.hero ) )
{
}

NotMovedCommand::~NotMovedCommand()
{
    std::cout << " ~NotMovedCommand " << std::endl;

}

void NotMovedCommand::execute()
{
    hero->notMoved();
}
