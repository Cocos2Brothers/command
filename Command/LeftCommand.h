#pragma once

#include <memory> 

#include "Command.h"
#include "Hero.h"

class LeftCommand :
    public Command
{
    std::shared_ptr <Hero> hero;
public:
    LeftCommand(std::shared_ptr <Hero> her);
    LeftCommand( LeftCommand && her ) = delete;
    LeftCommand( LeftCommand const & ) = delete;
    LeftCommand &operator= ( LeftCommand const & ) = delete;
    virtual ~LeftCommand();

    virtual void execute() override;
};

class BackCommand :
    public Command
{
    std::shared_ptr <Hero> hero;
public:
    BackCommand( std::shared_ptr <Hero> her );
    BackCommand( BackCommand && her );
    BackCommand( BackCommand const & ) = delete;
    BackCommand &operator= ( BackCommand const & ) = delete;
    virtual ~BackCommand();

    virtual void execute() override;
};

class RightCommand :
    public Command
{
    std::shared_ptr <Hero> hero;
public:
    RightCommand( std::shared_ptr <Hero> her );
    RightCommand( RightCommand && her );
    RightCommand( RightCommand const & ) = delete;
    RightCommand &operator= ( RightCommand const & ) = delete;
    virtual ~RightCommand();

    virtual void execute() override;
};

class ForwardCommand :
    public Command
{
    std::shared_ptr <Hero> hero;
public:
    ForwardCommand( std::shared_ptr <Hero> her );
    ForwardCommand( ForwardCommand && her );
    ForwardCommand( ForwardCommand const & ) = delete;
    ForwardCommand &operator= ( ForwardCommand const & ) = delete;
    virtual ~ForwardCommand();

    virtual void execute() override;
};

class NotMovedCommand :
    public Command
{
    std::shared_ptr <Hero> hero;
public:
    NotMovedCommand( std::shared_ptr <Hero> her );
    NotMovedCommand( NotMovedCommand && her );
    NotMovedCommand( NotMovedCommand const & ) = delete;
    NotMovedCommand &operator= ( NotMovedCommand const & ) = delete;
    virtual ~NotMovedCommand();

    virtual void execute() override;
};