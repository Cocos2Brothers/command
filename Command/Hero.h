#pragma once
class Hero
{
public:
    Hero();
    virtual ~Hero();
    
    void left();
    void back();
    void right();
    void forward();
    void notMoved();
};

