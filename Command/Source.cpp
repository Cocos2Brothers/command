#include <iostream>
#include <memory>

#include "CommandTest.h"

int main()
{
    std::unique_ptr <CommandTest> ctest( new CommandTest );

    ctest->simulate();

    system( "pause" );
    return 0;
}