#include "Hero.h"

#include <iostream>


Hero::Hero()
{
    std::cout << " Hero constr " << this << std::endl;
}


Hero::~Hero()
{
    std::cout << " Hero destr " << this << std::endl;
}

void Hero::left()
{
    std::cout << " Hero action left " << this << std::endl;
}

void Hero::back()
{
    std::cout << " Hero action back " << this << std::endl;

}

void Hero::right()
{
    std::cout << " Hero action right " << this << std::endl;

}

void Hero::forward()
{
    std::cout << " Hero action forward " << this << std::endl;

}

void Hero::notMoved()
{
    std::cout << " Hero action notMoved " << this << std::endl;

}
