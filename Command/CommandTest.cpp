#include "CommandTest.h"

#include <memory>

#include "InputHandler.h"
#include "Hero.h"
#include "Command.h"

CommandTest::CommandTest()
{
}


CommandTest::~CommandTest()
{
}

void CommandTest::simulate()
{
    //Hero * hero = new Hero;
    //InputHandler * inputHan = new InputHandler(hero);

    std::unique_ptr < Hero > hero( new Hero );
    std::unique_ptr <InputHandler > inputHan( new InputHandler(std::move ( hero ) ) );

    std::unique_ptr <Command> executor = inputHan->inputHandler( 'a' );
    executor->execute();
    executor = inputHan->inputHandler( 'a' );
    executor->execute();

    executor = inputHan->inputHandler( 's' );
    executor->execute();
    executor = inputHan->inputHandler( 'd' );
    executor->execute();
    executor = inputHan->inputHandler( 'w' );
    executor->execute();
    executor = inputHan->inputHandler( 'x' );


}
